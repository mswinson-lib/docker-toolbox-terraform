# toolbox-terraform

      image for building terraform configurations


## Usage

      docker run -it --rm --entrypoint /bin/sh mswinson/toolbox-terraform 

## Image Variants


### toolbox-terraform

packages  

      terraform
      make
      ruby
      test-kitchen
      kitchen-terraform
      kitchen-inspec
      git


