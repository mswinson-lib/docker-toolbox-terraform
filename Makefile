.PHONY: clean build style test deploy release

PROJECT_ROOT ?= `pwd`

BASEIMAGE=ruby
BASEIMAGE_TAG=2.3.7-alpine3.8
VERSION=$(file < VERSION)

REPO=mswinson
NAME=toolbox-terraform
ID=$(REPO)/$(NAME)
TAG=alpine3.8
TAG_SUFFIX ?= '-develop'

clean:
	docker rmi $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX)

build:
	@mkdir -p ./target
	@tar -C source -czf ./target/packer-build.tar.gz packer-build
	@packer build -var-file=variables.json -var image_tag=$(TAG)-$(VERSION)$(TAG_SUFFIX) template.json

style:
	@packer validate template.json

deploy:
	@docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@docker push $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX)

release:
	@docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@docker tag $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) $(ID):$(TAG)
	@docker tag $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) $(ID):latest
	@docker push $(ID):$(TAG)
	@docker push $(ID):latest

test:
	@docker run --rm -v $(PROJECT_ROOT):/var/workspace $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) ./test/smoke.sh
