**28/02/2019**

**v0.1.3**

    add git package

**25/02/2019**

**v0.1.2**

    readd support for running test kitchen


**25/10/2018**

**v0.1.1**

Fixes  

      fix image tagging to correctly include version number  



**v0.1.0**

      first version
